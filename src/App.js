import React from 'react';
import './App.css';
import { connect } from 'react-redux';
import TextBox from './Components/TextBox';
import View from './Components/View'
import {BrowserRouter as Router,Route} from "react-router-dom";
import AddItem from './Components/AddItem'
import Navbara from './Components/Navbar'



class App extends React.Component {

  render() {
    return (
      <Router>
      <div className="container">
<Navbara></Navbara>
      <Route path="/view" exact component={View}></Route>
      <Route path="/create" exact component={AddItem}></Route>
      
    
      </div>
      </Router>
    );
  }
}

export default App;