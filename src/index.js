import React from 'react'
import { render } from 'react-dom'
import {Provider} from 'react-redux'
import { createStore } from 'redux'
import reducer from './reducer'
import App from './App'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css'


const store = createStore(reducer)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)