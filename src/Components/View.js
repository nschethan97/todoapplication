import React, { Component } from 'react'
import { connect } from 'react-redux';
import TextBoxx from './TextBoxx';

export class View extends Component {
    onChangeHandler = (e) => {
        console.log("1")
        this.props.addItem(e.target.value)
      }
      deleteData = (index) =>{
        this.props.deleteData(index)
      }
      editItem = (index) =>{
        this.props.editItem(index)
      }
      showItemBox = () =>{
        this.props.showItemBox()
      }
      showList = () =>{
        this.props.showList()
      }
    render() {
        console.log(this.props.todoList)
        return (
            <div>
                <TextBoxx onChangeHandler={this.onChangeHandler} submit={this.props.submit} value={this.props.item} ></TextBoxx> 
           { this.props.viewList ?
              this.props.todoList.map((data,index)=>{
                return <div key={index } style={{border: "5px solid black", padding:"1px"}}>
                  <p>{data.name}</p>
                  <button onClick={()=> this.deleteData(index)}>delete</button>
                  <button onClick={()=> this.editItem(index)}>Edit</button></div>
              }) : null
            }
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
      item: state.item,
      todoList: state.todoList,
      itemBox: state.showItemBox,
      viewList: state.viewList,
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      addItem: (value) => dispatch({ type: "ADD_ITEM", value: value }),
      submit: () => dispatch({ type: "SUBMIT" }),
      deleteData: (index) => dispatch({ type: "DELETE_DATA", value: index }),
      editItem: (index) => dispatch({ type: "EDIT_ITEM", value: index}),
      showItemBox: () => dispatch({type:"SHOW_ITEM_BOX"}),
      showList: () => dispatch({type: "SHOW_LIST"})
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(View);

