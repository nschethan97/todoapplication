import React from 'react';

const TextBoxx = (props) => {
    return (
        <div>
            <input type="text" placeholder="Add Item" onChange={props.onChangeHandler} value={props.value}></input>
            <button onClick={props.submit}>Update Item</button>
        </div>
    )
}

export default TextBoxx;