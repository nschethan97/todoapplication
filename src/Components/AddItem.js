import React, { Component } from 'react'
import { connect } from 'react-redux';
import TextBox from './TextBox';

export class AddItem extends Component {
    onChangeHandler = (e) => {
        console.log("1")
        this.props.addItem(e.target.value)
      }
    showItemBox = () =>{
        this.props.showItemBox()
      }
    render() {
        return (
            <div>
            
            
               <TextBox onChangeHandler={this.onChangeHandler} submit={this.props.submit} value={this.props.item} ></TextBox> 
            

            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
      item: state.item,
      
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      addItem: (value) => dispatch({ type: "ADD_ITEM", value: value }),
      submit: () => dispatch({ type: "SUBMIT" }),
      
    }
  }
  export default connect(mapStateToProps, mapDispatchToProps)(AddItem);

