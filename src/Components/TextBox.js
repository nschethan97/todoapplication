import React from 'react';

const TextBox = (props) => {
    return (
        <div>
            <input type="text" placeholder="Add Item" onChange={props.onChangeHandler} value={props.value}></input>
            <button onClick={props.submit}>Add Item</button>
        </div>
    )
}

export default TextBox;