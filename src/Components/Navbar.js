import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import { connect } from 'react-redux';

import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem } from 'reactstrap';
 export class Navbara extends Component{
     
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this) ;
        this.state = {
          isOpen: false
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
      showItemBox = () =>{
        this.props.showItemBox()
      }
      showList = () =>{
        this.props.showList()
      }
    render(){
        return(     
                    <div className="navbr">
                     <Navbar color="dark"  expand="md">
                      
                    
                      <h1 class="navbar-text" className="t">TODO APP</h1> 
                      <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isO} navbar>
                     <Nav className="ml-auto" navbar>                    
                   
                    <NavItem className="nav-item">
                <NavLink href="" onClick={this.showItemBox}><Link to='/create' className="nav-item"><i class="fa fa-user-plus" ></i>ADD ITEM</Link></NavLink>
              </NavItem>
              <NavItem className="nav-item">
              <NavLink href="" onClick={this.showList}><Link to='/view' className="nav-item"><i class="fa fa-user-plus" ></i>VIEW ITEM</Link></NavLink>
            </NavItem>
                  
              </Nav>  
              </Collapse>
              </Navbar>
              
            </div>
        
        )
    }
}
const mapStateToProps = (state) => {
    return {
      item: state.item,
      todoList: state.todoList,
      itemBox: state.showItemBox,
      viewList: state.viewList,
    }
  }
  
  const mapDispatchToProps = (dispatch) => {
    return {
      addItem: (value) => dispatch({ type: "ADD_ITEM", value: value }),
      submit: () => dispatch({ type: "SUBMIT" }),
      deleteData: (index) => dispatch({ type: "DELETE_DATA", value: index }),
      editItem: (index) => dispatch({ type: "EDIT_ITEM", value: index}),
      showItemBox: () => dispatch({type:"SHOW_ITEM_BOX"}),
      showList: () => dispatch({type: "SHOW_LIST"})
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(Navbara);