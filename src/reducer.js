const initialState ={
    item:"",
    todoList:[],
    editClickedIndex: -1,
    showItemBox: false,
    viewList : false,
}

const reducer = (state = initialState, action) =>{
    var newState = {...state}
    switch(action.type){
        case "ADD_ITEM" : {
            console.log("2")
            newState.item = action.value;
            return newState
        }
        case "SUBMIT" : {
            var list = {name: newState.item} 
            var arr = newState.todoList;
            if(newState.editClickedIndex>= 0){
                arr[newState.editClickedIndex].name = newState.item
            }else{
                newState.todoList.push(list)
            }
            newState.editClickedIndex = -1
            newState.item = ""
            return newState
        }
        case "DELETE_DATA":{
            var list = newState.todoList.slice();
            list.splice(action.value, 1)
            newState.todoList = list
            return newState;
        }
        case "EDIT_ITEM":{
            var list = newState.todoList.slice();
            newState.item=list[action.value].name;
            newState.editClickedIndex = action.value
            return newState;
        }
        case "SHOW_ITEM_BOX":{
            newState.showItemBox = !newState.showItemBox;
            return newState
        }
        case "SHOW_LIST":{
            newState.viewList = !newState.viewList;
            return newState
        }
        default :
        return state
    }
}

export default reducer